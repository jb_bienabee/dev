from qtest.api import QTestAPI
from qtest.project import QTestProject
from qtest.test_case import QTestTestCase
from qtest.user import QTestUser
from qtest.module import QTestModule
import maya

# test flags
test_login = False
test_create_project = False
test_create_test_case = False
test_create_user = False
test_create_module = False

test_get_all_test_case_fields = False
test_get_module_by_id = False
test_get_test_case_by_id = False

test_assign_user_to_project = False

#######################################
# Release tests
test_get_all_releases = False
test_get_release_by_id = False
test_create_release = False
test_get_all_release_fields = False

# api variable
auth_token = "eb9611dc-4a82-4e47-bdb9-90f90d15e171"
project_id = 45794

api = QTestAPI()

api.set_auth_token(auth_token)

if (test_login):
    api.login()

if (test_create_project):
    project = QTestProject("Python Project",
                           "JBrokaw created this project with python",
                           maya.now().rfc3339(),
                           maya.when("1 year from now").rfc3339(),
                           ["joshua@bienabee.com"])

    api.create_project(project)

if (test_create_test_case):
    newTestCase = QTestTestCase("Python Test Case",
                                "JBrokaw created this test case with python")

    if (test_create_project):
        api.create_test_case(project.id, newTestCase)
    else:
        api.create_test_case(project_id, newTestCase)

if (test_create_user):
    user = QTestUser("test@fakefakeemails.com",
                     "Joshua",
                     "Brokaw",
                     "test@fakefakeemails.com",
                     "welcome1")

    api.create_user(user)

if (test_create_module):
    module1 = QTestModule("Python Module",
                          "JBrokaw created this module with python")

    api.create_module(module1, project_id)

    module2 = QTestModule("Nested Python Module",
                          "JBrokaw created this nested module with python",
                          parent_id=module1.id)

    api.create_module(module2, project_id)

if (test_get_module_by_id):
    api.get_module_by_id(module_id=2680835, project_id=project_id)

if (test_assign_user_to_project):
    api.assign_users_to_project()

if (test_get_all_test_case_fields):
    api.get_all_test_case_fields(project_id)

if (test_get_test_case_by_id):
    api.get_test_case_by_id(test_case_id=11824018, project_id=project_id)

if test_get_all_releases:
    api.get_all_releases(project_id)

if test_get_release_by_id:
    api.get_release_by_id(169551, project_id)

if test_get_all_release_fields:
    api.get_all_release_fields(project_id)
