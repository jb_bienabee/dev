import json
import requests
from requests.auth import HTTPBasicAuth

BASE_URL = 'https://bienabee.qtestnet.com'


class QTestAPI(object):
    """
    docstring for QTestAPI.
    """
    def __init__(self):
        pass

###############################################################################
    def set_auth_token(self, token):
        self.authHeader = {
                        'Authorization': 'bearer ' + token,
                        'Content-Type': 'application/json'
        }

###############################################################################
    def login(self):
        login_path = 'oauth/token'
        login_type = 'POST'
        loginData = {
                  'grant_type': 'password',
                  'username': 'joshua@bienabee.com',
                  'password': 'GxMGnK8VTvfR6uWl'
        }
        loginHeaders = {
                     'Content-Type': 'application/x-www-form-urlencoded'
        }

        r = requests.request(login_type,
                             BASE_URL + login_path,
                             data=loginData,
                             auth=HTTPBasicAuth('joshua@bienabee.com', ''),
                             headers=loginHeaders)

        responseData = json.loads(r.text)

        self.set_auth_token(responseData['access_token'])


###############################################################################
    def create_project(self, project):
        path = '/api/v3/projects'

        response = requests.post(BASE_URL + path,
                                 json=project.get_json_data(),
                                 headers=self.authHeader)

        parsed = json.loads(response.text)
        project.id = parsed['id']

###############################################################################
    def create_test_case(self, test_case, project_id):
        path = '/api/v3/projects/' + str(project_id) + '/test-cases'

        response = requests.post(BASE_URL + path,
                                 json=test_case.get_json_data(),
                                 headers=self.authHeader)

        parsed = json.loads(response.text)
        test_case.id = parsed['id']
        print json.dumps(parsed, indent=4)

###############################################################################
    def get_test_case_by_id(self, test_case=None,
                            test_case_id=None, project_id=None):
        if test_case is not None:
            test_case_id = test_case.id
            project_id = test_case.project_id

        path = ('/api/v3/projects/' + str(project_id) +
                '/test-cases/' + str(test_case_id))

        response = requests.get(BASE_URL + path,
                                headers=self.authHeader)

        assert(response.status_code == 200)
        print json.dumps(response.json(), indent=4)

###############################################################################
    def create_user(self, user):
        path = '/api/v3/users'

        response = requests.post(BASE_URL + path,
                                 json=user.get_json_data(),
                                 headers=self.authHeader)

        parsed = json.loads(response.text)
        print json.dumps(parsed, indent=4)
        user.id = parsed['id']

###############################################################################
    def create_module(self, module, project_id):
        path = '/api/v3/projects/' + str(project_id) + '/modules'
        param = '?parentId='

        if (module.parent_id is not None):
            param = param + str(module.parent_id)
            path = path + param

        module.project_id = project_id

        response = requests.post(BASE_URL + path,
                                 json=module.get_json_data(),
                                 headers=self.authHeader)

        parsed = response.json()
        module.id = parsed['id']
        print json.dumps(parsed, indent=4)

###############################################################################
    def get_module_by_id(self, module=None, module_id=None, project_id=None):
        if module is not None:
            module_id = module.id
            project_id = module.project_id

        path = ('/api/v3/projects/' + str(project_id) +
                '/modules/' + str(module_id))

        response = requests.get(BASE_URL + path,
                                headers=self.authHeader)

        assert(response.status_code == 200)
        print json.dumps(response.json(), indent=4)

###############################################################################
    def assign_users_to_project(self):
        path = '/api/v3/users/projects'

        data = {
            'project_id': 45794,
            'user_ids': [48379]
        }

        response = requests.post(BASE_URL + path,
                                 json=data,
                                 headers=self.authHeader)

        parsed = json.loads(response.text)
        print json.dumps(parsed, indent=4)

###############################################################################
    def get_all_test_case_fields(self, project_id):
        path = ('/api/v3/projects/' + str(project_id) +
                '/settings/test-cases/fields')

        response = requests.get(BASE_URL + path,
                                headers=self.authHeader)

        assert(response.status_code == 200)
        print json.dumps(response.json(), indent=4)

###############################################################################
    def create_custom_test_case_field(self, project_id):
        path = ('/api/v3/projects/' + str(project_id) +
                '/settings/test-cases/fields')

        data = {}

        response = requests.post(BASE_URL + path,
                                 json=data,
                                 headers=self.authHeader)

        parsed = json.loads(response.text)
        print json.dumps(parsed, indent=4)

###############################################################################
    def get_all_releases(self, project_id):
        path = '/api/v3/projects/' + str(project_id) + '/releases'

        response = requests.get(BASE_URL + path,
                                headers=self.authHeader)

        assert(response.status_code == 200)
        print json.dumps(response.json(), indent=4)

###############################################################################
    def get_release_by_id(self, release_id, project_id):
        path = ('/api/v3/projects/' + str(project_id) +
                '/releases/' + str(release_id))

        response = requests.get(BASE_URL + path,
                                headers=self.authHeader)

        assert(response.status_code == 200)
        print json.dumps(response.json(), indent=4)

###############################################################################
    def get_all_release_fields(self, project_id):
        path = ('/api/v3/projects/' + str(project_id) +
                '/settings/releases/fields')

        response = requests.get(BASE_URL + path,
                                headers=self.authHeader)

        assert(response.status_code == 200)
        print json.dumps(response.json(), indent=4)
