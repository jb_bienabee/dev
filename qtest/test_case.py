# https://support.qasymphony.com/hc/en-us/articles/205610505#Create A Test Case
# Name	        Required	Type	    Description
# name	        True	    String	    Test case name
# description	False	    String	    Test case description
# properties	True	    JSONArray	An array of field-value pairs.
# test_steps	False	    JSONArray	The JSONArray of TestStep objects
# parent_id	    False	    String	    ID of the parent module.


class QTestTestCase(object):
    """docstring for qtest_testcase."""
    def __init__(self,
                 name,
                 description,
                 properties=None,
                 test_steps=None,
                 parent_id=None):
        self.name = name
        self.description = description
        self.properties = [] if properties is None else properties
        self.test_steps = [] if test_steps is None else test_steps
        self.parent_id = parent_id
        self.project_id = None
        self.id = None

    def get_json_data(self):
        data = {
             'name': self.name,
             'description': self.description,
             'properties': self.properties
        }

        if not self.test_steps:
            data['test_steps'] = self.test_steps

        if self.parent_id is not None:
            data['parent_id'] = self.parent_id

        return data
