# https://support.qasymphony.com/hc/en-us/articles/204964959-1-Common-APIs
# Name	        Required	Type	    Description
# name	        True	    String	    Test case name
# description	False	    String	    Test case description
# properties	True	    JSONArray	An array of field-value pairs.
# test_steps	False	    JSONArray	The JSONArray of TestStep objects
# parent_id	    False	    String	    ID of the parent module.


class QTestUser(object):
    """docstring for qtest_user."""
    def __init__(self,
                 username,
                 first_name,
                 last_name,
                 email,
                 password):
        self.username = username
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.password = password
        self.id = None
        self.project_ids = []

    def get_json_data(self):
        data = {
             'username': self.username,
             'first_name': self.first_name,
             'last_name': self.last_name,
             'email': self.email,
             'password': self.password
        }

        return data
