class QTestProject(object):
    """docstring for QTestProject."""
    def __init__(self,
                 name,
                 description,
                 start_date,
                 end_date,
                 admins=None):
        self.name = name
        self.description = description
        self.start_date = start_date
        self.end_date = end_date
        self.admins = [] if admins is None else admins
        self.user_ids = []
        self.id = None

    def get_json_data(self):
        data = {
             'name': self.name,
             'description': self.description,
             'start_date': self.start_date,
             'end_date': self.end_date,
             'admins': self.admins
        }

        return data

    def get_json_user_data(self):
        data = {
            'project_id': self.id,
            'user_ids': self.user_ids
        }

        return data

    def add_user(self, user_id):
        self.user_ids.append(user_id)
