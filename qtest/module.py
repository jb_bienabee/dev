class QTestModule(object):
    """docstring for QTestProject."""
    def __init__(self,
                 name,
                 description,
                 parent_id=None,
                 shared=False):
        self.name = name
        self.description = description
        self.parent_id = parent_id
        self.shared = shared
        self.id = None
        self.project_id = None

    def get_json_data(self):
        data = {
            'name': self.name,
            'description': self.description
        }

        if (self.shared):
            data['shared'] = self.shared

        return data
